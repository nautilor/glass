# GLASS

Glass is a tool to make every window transparent


# Dependency

To work this program uses `wmctrl` and `xprop` so this two packages are needed


# Installation

    git clone https://gitlab.com/nautilor/glass.git
    cd glass
    chmod +x glass.py
    cp glass.py /usr/local/bin/glass

On the first run it will create an empty configuration under `~/.config/glass/glass.json`

the program will not run until at least one program is configured so let's see how to do that

# Configuration

Inside the config file just add something like this

    {
      "code-oss": {
        "match": "Code",
        "transparency": "0xf851eb84"
      },
      "spotify": {
        "match": "Spotify",
        "transparency": "0xf851eb84"
      }
    }

Automatically every `Visual Studio Code` and `Spotify` windows will receive transparency

The two important value here are `match` and `transparency`

## match

for the `match` part just run

    wmctrl -l

the otput should look something like this

        0x02000021 -1 hostname Plasma
        0x02e00021 -1 hostname Latte Dock
        0x01600001  0 hostname File - Project - Code - OSS
        0x01700001  0 hostname Manual - Project - Code - OSS
        0x03e00007  0 hostname ~ : zsh — Konsole

take any part after the hostname and use it as a match

## transparency

for the `transparency` just run 

    printf 0x%x $((0xffffffff * $opacity / 100)) 

> replace `$opacity` with a value between 0 and 100

below some examples

    0xdeb851ea # 87
    0xf851eb84 # 97
    0xcccccccc # 80
    0xb3333332 # 70

the otput from the previous command goes in the `transparency`