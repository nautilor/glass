#!/usr/bin/env python3

import subprocess
import json
import os
import time

BASE_DIR = os.path.join(os.path.expanduser('~'), '.config/glass/')
BASE_CONFIG = os.path.join(os.path.expanduser('~'), '.config/glass/glass.json')
WINDOWS_CONFIG = {}
OPACITY = "0xf851eb84" # 97
WINDOW_LIST_CMD = ['wmctrl', '-l']
WINDOW_OPACITY_CMD = 'xprop -id {} -format _NET_WM_WINDOW_OPACITY ' + \
                      '32c -set _NET_WM_WINDOW_OPACITY {}'

def load_config():
    global WINDOWS_CONFIG
    if not os.path.exists(BASE_DIR):
        print("Missing BASE_DIR, creating...")
        os.mkdir(BASE_DIR)
    if not os.path.exists(BASE_CONFIG):
        print("Missing BASE_CONFIG, creating...")
        with open(BASE_CONFIG, 'w')  as f:
            f.write('{}')

    WINDOWS_CONFIG = open(BASE_CONFIG, 'r')
    WINDOWS_CONFIG = json.load(WINDOWS_CONFIG)

def check_config():
    if not len(WINDOWS_CONFIG.keys()):
        print("There's currently no configured windows, quitting...")
        exit(-1)

def get_windows_list():
    wlist = subprocess.check_output(WINDOW_LIST_CMD)
    wlist = wlist.decode().split('\n')
    return [w for w in wlist if w]

def apply_to_matching(wlist):
    for window in WINDOWS_CONFIG:
        window = WINDOWS_CONFIG[window]
        print(f"parsing {window}")
        op = window['transparency'] if 'transparency' in window else OPACITY
        mlist = [w.split(' ')[0] for w in wlist if window['match'] in w]
        for match in mlist:
            cmd = WINDOW_OPACITY_CMD.format(match, op)

            cmd = cmd.split(' ')
            subprocess.Popen(cmd)


if __name__ == "__main__":
    load_config()
    check_config()
    while True:
        wlist = get_windows_list()
        apply_to_matching(wlist)
        time.sleep(3)
